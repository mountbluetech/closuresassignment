function cacheFunction(cb) {
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
    if(typeof cb !== "function") {
        throw new Error("cacheFunction accept only a function as argument");
    }
    let cache = {};
 
    return (...args) =>{
           let res = JSON.stringify(args);
            if(res in cache){
                return cache[res]; 
            }
            else{
                cache[res] = cb(...args);
                return cache[res];
            }
    }
}

module.exports = cacheFunction;