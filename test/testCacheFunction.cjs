let cf = require("../cacheFunction.cjs");

 let callback = (val) => {
    console.log(`invoked callback : ${val}`);
    return val*val;
};

let callCacheFunction = cf(callback);

// console.log(callCacheFunction([10,1,2,3,4,5,6,7,8,9,1,2,3,10]));
console.log(callCacheFunction(20));
console.log(callCacheFunction(10));
console.log(cf('hello'));