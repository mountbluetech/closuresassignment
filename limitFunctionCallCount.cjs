function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    let invokeCount = 0;
    if(typeof cb !== "function" || typeof n !== "number" || n<1 ) {
        throw new Error('Error : pass function first and positive number next');
        return null;
    }
    else 
    return function(...args){ 
        if(invokeCount < n){
            invokeCount++;
            return cb(...args);
        }
        else {
            console.log(null);
            return null;
        }
    }
}

module.exports = limitFunctionCallCount;