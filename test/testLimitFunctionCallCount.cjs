let functionCall = require("../limitFunctionCallCount.cjs");


let callback = (num)=>console.log(`callback invoked ${num}`);
let functionCallback;

try {
    functionCallback = functionCall(callback);
} catch (error) {
    console.log(error.message);
}

try {
    functionCallback = functionCall("Hello", 5);
} catch (error) {
    console.log(error.message);
}

try {
    functionCallback = functionCall(callback, 2);
} catch (error) {
    console.log(error.message);
}

functionCallback(10);
functionCallback(10);
functionCallback(10);
functionCallback(10);
functionCallback(10);

